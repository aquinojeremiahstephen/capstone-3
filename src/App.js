//dependencies
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
//css
import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './css/style.css';
//assets

//components
import NavBar_Comp from './components/NavBar_Comp';
import Footer_Comp from './components/Footer_Comp';
//pages
import Error from './pages/Error';
import Home_Page from './pages/Home_Page';
import Register_Page from './pages/Register_Page';
import Login_Page from './pages/Login_Page';
import Catalogue_Page from './pages/Catalogue_Page';
import Product_Page from './pages/Product_Page';
import Profile_Page from './pages/Profile_Page';
import Cart_Page from './pages/Cart_Page';
import Order_Page from './pages/Order_Page';
import Dashboard_Page from './pages/Dashboard_Page';
import Test_Page from './pages/Test_Page';
//usercontext
import { UserProvider } from './UserContext';


function App() {

    //global user state
    const [user, setUser] = useState({
        id: null,
        userIsAdmin: null,
        userFname: null,
        userLname: null
    });

    //function for unsetting the user
    const unsetUser = () => {
        localStorage.clear()
        setUser({
            id: null,
            userIsAdmin: null,
            userFname: null,
            userLname: null
        });
    }

    //Get user details again if the user reloads the page
    useEffect(() => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/profile`, {
            headers:{
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data._id !== undefined){
                setUser({
                    id: data._id,
                    userIsAdmin: data.userIsAdmin,
                    userFname: data.userFname,
                    userLname: data.userLname
                })
            }else{
                setUser({
                    id: null,
                    userIsAdmin: null,
                    userFname: null,
                    userLname: null
                })
            }
        })
    },[])

    return (
        //distribute global user state, and unsetUser (function)
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <NavBar_Comp/>
                    {/* Routes the page dependeing on ther 'to' url */}
                <Switch>
                    <Route exact path="/" component={Home_Page}/>
                    <Route exact path="/register" component={Register_Page}/>
                    <Route exact path="/login" component={Login_Page}/>
                    <Route exact path="/shop" component={Catalogue_Page}/>
                    <Route exact path="/shop/catalogue/:productId" component={Product_Page}/>
                    <Route exact path="/cart" component={Cart_Page}/>
                    <Route exact path="/profile" component={Profile_Page}/>
                    <Route exact path="/orders" component={Order_Page}/>
                    <Route exact path="/dashboard" component={Dashboard_Page}/>
                    <Route exact path="/test" component={Test_Page}/>
                    <Route component={Error}/>
                </Switch>
                <Footer_Comp/>
            </Router>
        </UserProvider>
    );
}

export default App;
