//dependencies
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Table, InputGroup } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2'
//usercontext
import UserContext from './../UserContext';
//assets
import CartEmpty from './../assets/cartEmpty.png'

//Cart Page
export default function Cart_Page(){

    //use to redirect user to a new path from react-router-dom
    const history = useHistory();

    //global user state + unsetUser function from App.js
    const { user } = useContext(UserContext);

    //set total amount state
    const [total, setTotal] = useState(0)

    //set state for cart
    const [cart, setCart] = useState([]);

    //set state for cart details
    const [cartDetails, setCartDetails] = useState([]);

    //set if there is existing cart and total
    useEffect(()=> {
        if(localStorage.getItem('cart')){
            setCart(JSON.parse(localStorage.getItem('cart')));
        }
    }, [])

    //function for updating total amount whenever our cart state changes, re-calculate the total
    useEffect(()=> {
        //start with a counter initialized to zero
        let tempTotal = 0

        //loop through our cart, getting each item's subtotal and incrementing our tempTotal counter by its amount
        cart.forEach((item)=> {
            tempTotal += item.productSubtotal
        })

        //set our total state
        setTotal(tempTotal)
    }, [cart])

    //will render new data when cart state changes
    useEffect(() => {
        const cartArr = cart.map(item => {
            return(     
                <tr key={item.productId}>
                    <td className="cart-product-name-cell">{item.productName} - {item.productCategory}</td>
                    <td className="cart-product-price-cell">PHP {item.productPrice}.00</td>
                    <td className="cart-product-quantity-cell">
                        <InputGroup>
                         
                            <Button type="button" className="btn cart-qty-button shadow-none" onClick={e => minusQty(item.productId)}>-</Button>
 
                          
                            <Form.Control min="1" className="cart-quantity-field shadow-none" type="number" value={item.productQty} onChange={e => qtyInput(item.productId,e.target.value)} required/>
                         
                            
                            <Button type="button" className="btn cart-qty-button shadow-none" onClick={e => addQty(item.productId)}>+</Button>
                           
                        </InputGroup>
                    </td>
                    <td className="cart-product-total-cell">PHP {item.productSubtotal}.00</td>
                    <td className="cart-product-remove-cell"><Button className="cart-remove-item-button" variant="danger" onClick={e => {removeItem(item.productId)}}>X</Button></td>
                </tr>
            );
        })

        setCartDetails(cartArr);
    }, [cart])

    //function for removing item from cart
    const removeItem = (productId) => {
        //use the spread operator to create a temporary copy of our cart (from the cart state)
        let tempCart = [...cart];
        
        //loop and find the item with the matching product id you want to remove
        for(let i = 0; i < tempCart.length ; i++){
            //use splice to remove the item we want from our cart
            if(tempCart[i].productId === productId){
                tempCart.splice([i], 1)
            }
        }

        //set our cart state with the new quantities
        setCart(tempCart)

        //set our localStorage cart as well
        localStorage.setItem('cart', JSON.stringify(tempCart))	
    }

    //function to add quantity to selected product
    const addQty = (productId) => {
        let tempCart = [...cart];

        for(let i = 0; i < tempCart.length; i++){
            if(tempCart[i].productId === productId ){
                tempCart[i].productQty += 1;
                tempCart[i].productSubtotal = tempCart[i].productPrice * tempCart[i].productQty;
            }
        }

        //set our cart state with the new quantities
        setCart(tempCart)

        //set our localStorage cart as well
        localStorage.setItem('cart', JSON.stringify(tempCart))
    }

    //function to minus quantity to selected product
    const minusQty = (productId) => {
        let tempCart = [...cart];

        for(let i = 0; i < tempCart.length; i++){
            if(tempCart[i].productId === productId ){
                tempCart[i].productQty -= 1;
                tempCart[i].productSubtotal = tempCart[i].productPrice * tempCart[i].productQty;
            }
        }

        //set our cart state with the new quantities
        setCart(tempCart)

        //set our localStorage cart as well
        localStorage.setItem('cart', JSON.stringify(tempCart))
    }

    //function for adjusting quantitiy base on input
    const qtyInput = (productId, value) => {

        //use the spread operator to create a temporary copy of our cart (from the cart state)
        let tempCart = [...cart]

        //loop through our tempCart 
        for(let i = 0; i < tempCart.length; i++){
            //so that we can find the item with the quantity we want to change via its productId
            if(tempCart[i].productId === productId){
                //use parseFloat to make sure our new quantity will be parsed as a number
                tempCart[i].productQty = parseFloat(value)
                //set the new subtotal
                tempCart[i].productSubtotal = tempCart[i].productPrice * tempCart[i].productQty
            }
        }

        //set our cart state with the new quantities
        setCart(tempCart)

        //set our localStorage cart as well
        localStorage.setItem('cart', JSON.stringify(tempCart))	
    }

    //function for checking out cart and sending it to db
    const checkOutCart = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/orders/cart/checkOut`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                orderSummary: cart,
		        orderTotal: total,
                orderBuyerFname: user.userFname,
                orderBuyerLname: user.userLname
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: 'Order placed',
                    icon: 'success',
                    text: "Thank you for your patronage!"
                });

                localStorage.removeItem('cart');

                history.push('/shop')
            }else{
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: "Please try again"
                });
            }
        })
    }

    //conditional rendering if cart is empty
    let cartRender = (cart.length === 0) ? (
        <>
           <div className="cart-empty-container">
               <img src={CartEmpty} className="cart-empty-img"/>
           </div>
        </>
    ) : (
        <>
            <Container className="cart-container">
                <h3 className="cart-title">Shopping Cart</h3>
                <Table striped bordered hover responsive className="cart-table">
                        <thead className="cart-headers">
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartDetails}
                        </tbody>
                </Table>
                <div className="cart-checkout-container">
                    <InputGroup className="cart-checout-group">
                        <Form.Control
                            type="text" disabled value={`Total: PHP ${total}.00`}
                        />
                        <button className="cart-checkout-button" onClick={e => checkOutCart()}>Check out</button>
                    </InputGroup>
                </div>
            </Container>
        </>
    )
    //renders our cart
    return(
        <>
            {cartRender}
        </>
    );
}