//dependencies
import { useState, useEffect, useContext } from 'react';
import { Container, Spinner } from 'react-bootstrap';
//components
import Catalogue_Comp from './../components/Catalogue_Comp';
import Featured_Comp from './../components/Featured_Comp';
//user context
import UserContext from '../UserContext';

export default function Product_Page(){

    //set loading spinner
    const [loading, setLoading] = useState(true);
    const [catalogueScreen, setCatalogueScreen] = useState(undefined);
    //global user state
    const { user } = useContext(UserContext);

    //set state for rendering, setter will set when a data is successfuly fetched
    const [products, setProducts] = useState([]);
    const [featured, setFeatured] = useState([]);
    
    //fetch data of products to be pass as props
    const fetchData = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/products/catalogue/all`)
        .then(res => res.json())
        .then(data => {
            setProducts(data);
            //radIndex picks a random index from the fetched array to be the featured product
            let randIndex =  Math.floor(Math.random() * ((data.length) - 1) + 0);
            setFeatured(data[randIndex]);
            setLoading(false);
        })
    }

    //refetch data when the user reloads
    useEffect(() => {
        fetchData();
    },[])

    useEffect(() => {
        //conditional rendering for navbar is the user is logged in
        let screen = (!loading) ? (
            <>
                <Featured_Comp featuredProp={featured}/>
                <Catalogue_Comp productsData={products} fetchData={fetchData}/>
            </>
        ) : (
            <>
                <div className="spinner-container d-flex align-items-center justify-content-center">
                    <Spinner animation="border" size="lg" className="spinner-container-circle"/>
                </div>
            </>
        )

        setCatalogueScreen(screen)
    },[loading]);

    

    //Creates catalogue page
    return(
        <>  
            {catalogueScreen}
        </>
    );
}