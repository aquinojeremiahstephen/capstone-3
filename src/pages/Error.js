import { Jumbotron, Button, Container } from "react-bootstrap";

export default function Error(){

    return(
        <Jumbotron fluid className="error-image">
            <Container>
                <h1 className="error-title-responsive">404 - Not Found</h1>
                <p className="error-caption-responsive banner-caption-container mt-5">
                    Oops! I think your on the wrong page  
                </p>
            </Container>
    </Jumbotron>
    );
}