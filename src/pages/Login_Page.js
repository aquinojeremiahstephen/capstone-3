//dependencies
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2'
//usercontext
import UserContext from './../UserContext';

//Login Page
export default function Login_Page(){

    //fields state
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [loginButton, setLoginButton] = useState(false);

    //global user state
    const { user, setUser } = useContext(UserContext);

    //login user function
    const LoginUser = (e) => {
        e.preventDefault();

        //fetch from DB with API
        fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userEmail: userEmail,
                userPassword: userPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== "undefined"){
                //set the token value on localStorage
                localStorage.setItem('token', data.access);
                //retrieve user details from DB via token
                retrieveUserDetails(data.access);
            }else{
                Swal.fire({
                    title: 'Authentication failed',
                    icon: 'error',
                    text: "Check your login details and try again"
                });
           }
        })
    }

    //retrieve user detials function (id, iAdmin, etc)
    const retrieveUserDetails = (token) => {
        //fetch user details to DB using the bearer token
        fetch(`${ process.env.REACT_APP_API_URL }/users/profile`, {
            headers:{
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //sets the global user state to be pass arround to determine the users ID and Admin rights
            setUser({
                id: data._id,
                userIsAdmin: data.userIsAdmin,
                userFname: data.userFname,
                userLname: data.userLname
            })
        })
    }

    //disable button via useeffect if credentials is not valid
    useEffect(() => {
        if(userEmail !== '' && userPassword !== '' ){
            setLoginButton(true);
        }else{
            setLoginButton(false);
        }
    },[userEmail, userPassword]);
    

    //redirect if already logged in
    if(user.id != null){
        return <Redirect to="/"/>
    }
    
    //Render the form with two way binding
    return(
        <div className="login-container">
            <div className="login-card">
                <p className="login-title">Sign In</p>
                <Form onSubmit={ (e) => LoginUser(e)}>
                    <Form.Group className="login-fields">
                        <Form.Control className="login-form-control shadow-none" type="email" placeholder="Enter email" value={userEmail} onChange={e => setUserEmail(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="login-fields">
                        <Form.Control className="login-form-control shadow-none" type="password" placeholder="Enter password" value={userPassword} onChange={e => setUserPassword(e.target.value)} required/>
                    </Form.Group>
                    <div className="login-btn-container">
                        {loginButton ?
                            <button className="login-button"  type="submit">Submit</button>
                            :  
                            <button className="login-button-disabled"  type="submit" disabled>Submit</button>
                        } 
                    </div>
                </Form>
            </div>
        </div>
    );
}