import Banner_Comp from './../components/Banner_Comp';
import Factors_Comp from './../components/Factors_Comp';
import About_Comp from './../components/About_Comp';
import './../css/style.css';


export default function Home(){
    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Oppurtunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll now!"
    }

    return(
        <>
            <Banner_Comp/>
            <Factors_Comp/>
            <About_Comp/>
        </>
    );
}