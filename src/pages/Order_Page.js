//dependencies
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Form, Row, Col } from 'react-bootstrap';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
//components
import Order_Comp from './../components/Order_Comp'
//usercontext
import UserContext from './../UserContext';

//Creates the order page
export default function Order_Page(){

    //global user state
    const { user } = useContext(UserContext);

    //set state for rendering, setter will set when a data is successfuly fetched
    const [toConfirmOrders, setToConfirmOrders] = useState([]);
    const [toShipOrders, setToShipOrders] = useState([]);
    const [shippedOrders, setShippedOrders] = useState([]);
    const [deliveredOrders, setDeliveredOrders] = useState([]);
    const [cancelledOrders, setCancelledOrders] = useState([]);

    //set state for the orders array to be passed as props
    const [toConfirmItems, setToConfirmItems] = useState([]);
    const [toShipItems, setToShipItems] = useState([]);
    const [shippedItems, setShippedItems] = useState([]);
    const [deliveredItems, setDeliveredItems] = useState([]);
    const [cancelledItems, setCancelledItems] = useState([]);
    
    //fetch data of user's orders to be pass as props
    const fetchToConfirm = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/orders/toConfirm`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setToConfirmOrders(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {

        //when the this component renders it will loop for each item on 'orders state' to get orders
        //the loop will repeat if the orders state prpop changes its value
        const toConfirmOrdersArr = toConfirmOrders.map(order => {
            return(
                //create the orders card using Order_Comp
                <Order_Comp ordersProp={order} key={order._id}/>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setToConfirmItems(toConfirmOrdersArr);

    },[toConfirmOrders])

    //fetch data of user's orders to be pass as props
    const fetchToShip = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/orders/toShip`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setToShipOrders(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {

        //when the this component renders it will loop for each item on 'orders state' to get orders
        //the loop will repeat if the orders state prpop changes its value
        const toShipOrdersArr = toShipOrders.map(order => {
            return(
                //create the orders card using Order_Comp
                <Order_Comp ordersProp={order} key={order._id}/>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setToShipItems(toShipOrdersArr);

    },[toShipOrders])

    //fetch data of user's orders to be pass as props
    const fetchShipped = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/orders/shipped`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setShippedOrders(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {

        //when the this component renders it will loop for each item on 'orders state' to get orders
        //the loop will repeat if the orders state prpop changes its value
        const shippedOrdersArr = shippedOrders.map(order => {
            return(
                //create the orders card using Order_Comp
                <Order_Comp ordersProp={order} key={order._id}/>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setShippedItems(shippedOrdersArr);

    },[shippedOrders])

    //fetch data of user's orders to be pass as props
    const fetchDelivered = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/orders/delivered`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setDeliveredOrders(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate....
    useEffect(() => {

        //when the this component renders it will loop for each item on 'orders state' to get orders
        //the loop will repeat if the orders state prpop changes its value
        const deliveredOrdersArr = deliveredOrders.map(order => {
            return(
                //create the orders card using Order_Comp
                <Order_Comp ordersProp={order} key={order._id}/>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setDeliveredItems(deliveredOrdersArr);

    },[deliveredOrders])

    //fetch data of user's orders to be pass as props
    const fetchCancelled = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/orders/cancelled`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setCancelledOrders(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {

        //when the this component renders it will loop for each item on 'orders state' to get orders
        //the loop will repeat if the orders state prpop changes its value
        const cancelledOrdersArr = cancelledOrders.map(order => {
            return(
                //create the orders card using Order_Comp
                <Order_Comp ordersProp={order} key={order._id}/>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setCancelledItems(cancelledOrdersArr);

    },[cancelledOrders])

    //refetch data when the user reloads
    useEffect(() => {
        fetchToConfirm();
        fetchToShip();
        fetchShipped();
        fetchDelivered();
        fetchCancelled();
    },[])

    //creeates order cards
    return(
        <Container className="order-main-container">
            <Tabs>
                <TabList>
                    <Tab>To confirm</Tab>
                    <Tab>To ship</Tab>
                    <Tab>Shipped</Tab>
                    <Tab>Delivered</Tab>
                    <Tab>Cancelled</Tab>
                </TabList>

                <TabPanel>
                    <Row className="order-cards-container">
                        {toConfirmItems}
                    </Row>
                </TabPanel>
                <TabPanel>
                    <Row className="order-cards-container">
                        {toShipItems}
                    </Row>
                </TabPanel>
                <TabPanel>
                    <Row className="order-cards-container">
                        {shippedItems}
                        </Row>
                </TabPanel>
                <TabPanel>
                    <Row className="order-cards-container">
                        {deliveredItems}
                        </Row>
                </TabPanel>
                <TabPanel>
                    <Row className="order-cards-container">
                        {cancelledItems}
                    </Row>
                </TabPanel>
            </Tabs>
        </Container>
    );
}