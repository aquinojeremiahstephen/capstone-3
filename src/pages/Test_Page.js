import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Test () {

    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productCategory, setProductCategory] = useState('');
    const [productPrice, setProductPrice] = useState(0);
    const [productStock, setProductStock] = useState(0);
    const [fileName, setFileName] = useState('');



    const addProduct = (e) => {
        
        e.preventDefault()
        
        let formData = new FormData();

        formData.append("productName", productName);
        formData.append("productDescription", productDescription);
        formData.append("productCategory", productCategory);
        formData.append("productPrice", productPrice);
        formData.append("productStock", productStock);
        formData.append("productImage", fileName);

        console.log(formData);

        fetch(`${ process.env.REACT_APP_API_URL }/admin/catalogue/add`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: formData
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Product added."
                });
                setProductName("");
                setProductDescription("");
                setProductPrice(0);
                setProductStock(0);

                
            }else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            }
        })
    }

    return(
        <div className="register-container">
            <div className="register-card">
                <p className="registration-title">Add Product Test</p>
                <Form  onSubmit={(e) => addProduct(e)} encType="multipart/form-data">
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Product Name" value={productName} onChange={e => setProductName(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" as="textarea" placeholder="Enter Description" value={productDescription} onChange={e => setProductDescription(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Category" value={productCategory} onChange={e => setProductCategory(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="number" placeholder="Enter Stock" value={productStock} onChange={e => setProductStock(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="number" placeholder="Enter Proce" value={productPrice} onChange={e => setProductPrice(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control type="file" filename="productImage" onChange={e => setFileName(e.target.files[0])} required/>
                    </Form.Group>
                    <div className="register-btn-container">
                        <button className="register-button"  type="submit">Submit</button>
                    </div>
                </Form>
            </div>
        </div>
    );
}