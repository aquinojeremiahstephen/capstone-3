//dependencies
import { useContext } from 'react';
import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, Redirect } from 'react-router-dom';
//components
import ProductDashboard_Comp from './../components/ProductDashboard_Comp';
import OrderDashboard_Comp from './../components/OrderDashboard_Comp';
//usercontext
import UserContext from './../UserContext';

export default function DashBoard_Page (){

     //global user state + unsetUser function from App.js
     const { user } = useContext(UserContext);

     //user redirect if logged in
    if(!user.userIsAdmin){
        return <Redirect to="/"/>
    }

    return(
        <Container className="dashboard-parent-container">
            <ProductDashboard_Comp/>
            <OrderDashboard_Comp/>
        </Container>
    );
}