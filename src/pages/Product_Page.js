//dependencies
import { useContext, useEffect, useState } from 'react';
import React from "react";
import { Container, Card, Button, Form, Row, Col, Modal } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import ReactStars from "react-rating-stars-component";
//usercontext
import UserContext from './../UserContext';

//Creates the spicific product page
export default function Product_Page(){

    //use to redirect user to a new path from react-router-dom
    const history = useHistory();

    //global user state
    const { user } = useContext(UserContext);

    //useParams() contains any values we are trying to pass in the URL stored in a key value pair
    //useParams() is how we recieve the courseId passed via the URL
    const { productId } = useParams();

    //set product details states
    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productCategory, setProductCategory] = useState('');
    const [productPrice, setProductPrice] = useState(0);
    const [productImage, setProductImage] = useState('');
    // const history = useHistory();

    //fetch product data on mount
    const fetchProducts = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/products/catalogue/profile/${productId}`)
        .then(res => res.json())
        .then(data => {

            //function for converting binary from db image field to image src
            const arrayBufferToBase64 = (buffer) => {
                var binary = '';
                var bytes = [].slice.call(new Uint8Array(buffer));
                bytes.forEach((b) => binary += String.fromCharCode(b));
                return window.btoa(binary);
            };
            
            //assign src string
            const imgSrc = `data:${data.img.contentType};base64,${arrayBufferToBase64(data.img.data.data)}`;

            setProductName(data.productName);
            setProductDescription(data.productDescription);
            setProductCategory(data.productCategory);
            setProductPrice(data.productPrice);
            setProductImage(imgSrc);
        })
    }

    //fetch product on reload
    useEffect(() => {
        fetchProducts();
    },[]);

    //set state for cart
    const [cart, setCart] = useState([])
    //set intial product quantity to be added to cart
    const [productQty, setProductQty] = useState(1);

    //set if there is existing cart and total
    useEffect(()=> {
        if(localStorage.getItem('cart')){
            setCart(JSON.parse(localStorage.getItem('cart')))
        }
    }, [])

    //function for adding item to cart
    const addToCart = (e) => {

        e.preventDefault();
        //variable to determine if the item we are adding is already in our cart or not
        let alreadyInCart = false
        //variable for the item's index in the cart array, if it already exists there
        let productIndex
        //temporary cart array
        let cart = []

        //if there is an existing item on cart get items
        if(localStorage.getItem('cart')){
            cart = JSON.parse(localStorage.getItem('cart'))
        }

        //loop through our cart to check if the item we are adding is already in our cart or not
        for(let i = 0; i < cart.length; i++){
            if(cart[i].productId === productId){
                //if it is, make alreadyInCart true
                alreadyInCart = true
                productIndex = i
            }
        }

        //if a product is already in our cart, just increment its quantity and adjust its subtotal
        if(alreadyInCart){
            cart[productIndex].productQty += productQty
            cart[productIndex].productSubtotal = cart[productIndex].productPrice * cart[productIndex].productQty
        }else{
            //else add a new entry in our cart, with values from states that need to be set wherever this function goes
            cart.push({
                'productId' : productId,
                'productName': productName,
                'productCategory': productCategory,
                'productPrice': productPrice,
                'productQty': productQty,
                'productSubtotal': productPrice * productQty
            })		
        }

        //set our cart state
        setCart(cart);
        //set our localStorage cart as well
        localStorage.setItem('cart', JSON.stringify(cart))

        Swal.fire({
            title: 'Product added to cart',
            icon: 'success',
            text: "Thank you for your patronage!"
        });

        history.push('/shop')
    }

    //conditional rendering for add to cart button
    let addToCartButton = (!user.id) ? (
        <>  
            <h6>You need to log in to order our products</h6>
            <button className="product-cart-button" type="submit" disabled>Add to cart</button>
        </>
    ) : (

        (user.userIsAdmin) ? (
            <></>
        )
        :
        (
            <>
                <button className="product-cart-button" type="submit">Add to cart</button>
            </>
        ) 
    )


    return(
        <Container>
            <Row className="product-main-container">
                <Col xs={12} md={12} lg={5}>
                    <div className="product-img-container">
                        <img className="product-img" src={productImage}/>
                    </div>
                    
                </Col>
                <Col xs={12} md={12} lg={7}>
                    
                    <p className="product-title">{productName}</p>
                    <p className="product-category">{productCategory}</p>
                    <p className="product-description">{productDescription}</p>
                    <p className="product-price">PHP {productPrice}.00</p>
                    <Form onSubmit={ (e) =>addToCart(e)}>   
                        <Form.Group>
                            <div className="d-flex flex-row">
                            <div>
                                <button type="button" className="btn product-minus"onClick={e => {
                                    setProductQty(productQty-1);
                                }}>-</button>
                            </div>
                            <Form.Control min="1" className="product-quantity-field shadow-none" type="number" value={productQty} onChange={e => {
                                let qty = parseFloat(e.target.value);
                                setProductQty(qty);
                            }} required/>
                            <div>
                                <button type="button" className="btn product-add" onClick={e => {
                                    setProductQty(productQty+1);
                                }}>+</button>
                            </div>
                            </div>
                        </Form.Group>
                        <div className="product-cart-button-container">
                            {addToCartButton}
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}