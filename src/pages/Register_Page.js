//dependencies
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
//usercontext
import UserContext from './../UserContext';

export default function Register_Page(){

    //field states
    const [userFname, setUserFname] = useState('');
    const [userLname, setUserLname] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userMobile, setUserMobile] = useState('');
    const [userAddress, setUserAddress] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');

    //usercontext state
    const { user, setUser } = useContext(UserContext);
    const history = useHistory();

    //register button state
    const [registerButton, setRegisterButton] = useState(false);

    //register user function
    function registerUser(e){

        e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userEmail: userEmail
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: 'Email already exist!',
                    icon: 'error',
                    text: "Please provide another email"
                });
            }else{
                fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        userFname: userFname,
                        userLname: userLname,
                        userMobile: userMobile,
                        userAddress: userAddress,
                        userEmail: userEmail,
                        userPassword: userPassword
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data === true){
                        Swal.fire({
                            title: 'Registration successful!',
                            icon: 'success',
                            text: "Welcome to Brewing Panda"
                        });

                        history.push('/login');
                    }else{
                        Swal.fire({
                            title: 'Something went wrong',
                            icon: 'error',
                            text: "Please try again"
                        }); 
                    }
                })
            }
        })
        
    }
          

    //conditional rendering for submit button
    useEffect(() => {
        if((userEmail !== '' && userPassword !== '' && verifyPassword !== '') && (userPassword === verifyPassword)){
            setRegisterButton(true);
        }else{
            setRegisterButton(false);
        }
    },[userEmail, userPassword, verifyPassword])

    //user redirect if logged in
    if(user.id != null){
        return <Redirect to="/"/>
        
    }

    //render the registration page
    return(
        <div className="register-container">
            <div className="register-card">
                <p className="registration-title">Registration</p>
                <Form onSubmit={ (e) => registerUser(e)}>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter First Name" value={userFname} onChange={e => setUserFname(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Last Name" value={userLname} onChange={e => setUserLname(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Mobile No." value={userMobile} onChange={e => setUserMobile(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" as="textarea" placeholder="Enter Address" value={userAddress} onChange={e => setUserAddress(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="email" placeholder="Enter email" value={userEmail} onChange={e => setUserEmail(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="password" placeholder="Enter password" value={userPassword} onChange={e => setUserPassword(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="register-fields">
                        <Form.Control className="register-form-control shadow-none" type="password" placeholder="Verify password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
                    </Form.Group>
                    <div className="register-btn-container">
                        {registerButton ?
                            <button className="register-button"  type="submit">Submit</button>
                            :  
                            <button className="register-button-disabled"  type="submit" disabled>Submit</button>
                        } 
                    </div>
                </Form>
            </div>
        </div>
    );
}