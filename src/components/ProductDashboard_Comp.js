//dependencies
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Form, Row, Col, Table, Modal } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
//usercontext
import UserContext from '../UserContext';

//Creates the specific product page
export default function ProductDashboard_Comp(){
    
    //set state for product data fetched
    const [productsData, setProductsData] = useState([]);
    const [productsItems, setProductsItems] = useState([]);

    //modal state
    const [showProductEdit, setShowProductEdit] = useState(false);
    const [showProductAdd, setShowProductAdd] = useState(false);

    //set edit and add form state
    const [productId, setProductId] = useState('');
    const [fileName, setFileName] = useState('');
    const [productName, setProductName] = useState('');
    const [productCategory, setProductCategory] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productStock, setProductStock] = useState(0);
    const [productPrice, setProductPrice] = useState(0);
    
    //function for product data fetching
    const fetchProducts = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/catalogue/all`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProductsData(data);
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'productsData state' to get products
        //the loop will repeat if the products data state prpop changes its value
        const productsDataArr = productsData.map(product => {

            // //generate the image url by converting binary data fetch from db to ascii
            // const arrayBufferToBase64 = (buffer) => {
            //     var binary = '';
            //     var bytes = [].slice.call(new Uint8Array(buffer));
            //     bytes.forEach((b) => binary += String.fromCharCode(b));
            //     return window.btoa(binary);
            // };
            
            // //assign src string
            // const imgSrc = `data:${product.img.contentType};base64,${arrayBufferToBase64(product.img.data.data)}`;

            return(
                //create data tablesp
                <tr key={product._id}>
                    {/* <td><img src={imgSrc} width={50}/></td> */}
                    <td className="dashboard-product-table-cells">{product.productName}</td>
                    <td className="dashboard-product-table-cells">{product.productCategory}</td>
                    <td className="dashboard-product-table-cells">{product.productDescription}</td>
                    <td className="dashboard-product-table-cells">{product.productStock}</td>
                    <td className="dashboard-product-table-cells">{product.productPrice}</td>
                    <td className="dashboard-product-table-cells cell-center">
                        {(product.productStatus === "Active") ?
                            <button className="dashboard-active-button" onClick={() => {
                                archiveToggle(product._id, product.productStatus)
                            }}>{product.productStatus}</button>
                        :
                            <button className="dashboard-inactive-button" onClick={() => {
                                archiveToggle(product._id, product.productStatus)
                            }}>{product.productStatus}</button>
                        }
                    </td>
                    <td className="dashboard-product-table-cells cell-center">
                        <button className="dashboard-update-button" onClick={() => openEdit(product._id)}>Update</button>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of components using 'Order_Comp' to generate product cards
        setProductsItems(productsDataArr);

    },[productsData])

    //function for toggling active/inactive products
    const archiveToggle = (productId, status) => {
        fetch(`${ process.env.REACT_APP_API_URL }/admin/catalogue/archive/${productId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productStatus: status
            })
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, which makes our component re render
            if(data === true){
                fetchProducts();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Product successfully archived/unarchived."
                });
            }else{
                fetchProducts();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //function for opening and closing modal edit and resetting it to blank after use
    const openEdit = (productId) => {
        // console.log(courseId);
        fetch(`${ process.env.REACT_APP_API_URL }/products/catalogue/profile/${ productId }`)
        .then(res => res.json())
        .then(data => {

            setProductId(data._id);
            setProductName(data.productName);
            setProductCategory(data.productCategory);
            setProductDescription(data.productDescription);
            setProductStock(data.productStock);
            setProductPrice(data.productPrice);

        })

        setShowProductEdit(true);
    }

    const closeEdit = () => {
        setShowProductEdit(false);

        setProductId('');
        setProductName('');
        setProductCategory('');
        setProductDescription('');
        setProductStock(0);
        setProductPrice(0);
    }

    //function for updating product
    const editProduct = (e, productId) => {
        e.preventDefault()

        fetch(`${ process.env.REACT_APP_API_URL }/admin/catalogue/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productName: productName,
                productCategory: productCategory,
                productDescription: productDescription,
                productStock: productStock,
                productPrice: productPrice
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                fetchProducts();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Course updated."
                });
                closeEdit();
            }else{
                fetchProducts();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            }
        })
    }

    //function for opening and closing add modal
    const openAdd = () => setShowProductAdd(true);
    const closeAdd = () => setShowProductAdd(false);

    //function for adding new product
    const addProduct = (e) => {
        
        e.preventDefault()
        
        let formData = new FormData();

        formData.append("productName", productName);
        formData.append("productDescription", productDescription);
        formData.append("productCategory", productCategory);
        formData.append("productPrice", productPrice);
        formData.append("productStock", productStock);
        formData.append("productImage", fileName);

        console.log(formData);

        fetch(`${ process.env.REACT_APP_API_URL }/admin/catalogue/add`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: formData
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Product added."
                });
                setProductName("");
                setProductCategory("");
                setProductDescription("");
                setProductPrice(0);
                setProductStock(0);
                setFileName('');

                fetchProducts();
                closeAdd();
                
            }else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });

                setProductName("");
                setProductCategory("");
                setProductDescription("");
                setProductPrice(0);
                setProductStock(0);
                setFileName('');

                fetchProducts();
                closeAdd();
            }
        })
    }


    //refetch if reloaded
    useEffect(() => {
        fetchProducts();
    },[]);
    
    //renders product dashboard comp
    return(
        <Container>
            <div className="d-flex justify-content-end">
                <button className="dashboard-product-add-button" onClick={openAdd}>Add Product</button>
            </div>
            <Table striped bordered hover responsive className="dashboard-product-table">
                <thead className="dashboard-product-table-headers">
                    {/* <th>Thumbnail</th> */}
                    <th>Product</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Stock</th>
                    <th>Price</th>
                    <th>Availability</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    {productsItems}
                </tbody>
            </Table>
            <br>
            </br>
            <br>
            </br>
            <br>
            </br>
            {/* EDIT MODAL */}
            <Modal show={showProductEdit} onHide={closeEdit}>
                <Form encType="multipart/form-data" onSubmit={e => editProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group controlId="productName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" value={productName} onChange={e => setProductName(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productCategory">
                                <Form.Label>Category</Form.Label>
                                <Form.Control type="text" value={productCategory} onChange={e => setProductCategory(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productDescription">
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" value={productDescription} onChange={e => setProductDescription(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productStock">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control type="number" value={productStock} onChange={e => setProductStock(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="number" value={productPrice} onChange={e => setProductPrice(e.target.value)} required/>
                            </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            {/* ADD MODAL */}
            <Modal show={showProductAdd} onHide={closeAdd}>
                <Form  onSubmit={(e) => addProduct(e)} encType="multipart/form-data">
                    <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        
                            <Form.Group className="register-fields">
                                <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Product Name" value={productName} onChange={e => setProductName(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group className="register-fields">
                                <Form.Control className="register-form-control shadow-none" as="textarea" placeholder="Enter Description" value={productDescription} onChange={e => setProductDescription(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group className="register-fields">
                                <Form.Control className="register-form-control shadow-none" type="text" placeholder="Enter Category" value={productCategory} onChange={e => setProductCategory(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group className="register-fields">
                                <Form.Control className="register-form-control shadow-none" type="number" placeholder="Enter Stock" value={productStock} onChange={e => setProductStock(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group className="register-fields">
                                <Form.Control className="register-form-control shadow-none" type="number" placeholder="Enter Proce" value={productPrice} onChange={e => setProductPrice(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="file" filename="productImage" onChange={e => setFileName(e.target.files[0])} required/>
                            </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
    )
}