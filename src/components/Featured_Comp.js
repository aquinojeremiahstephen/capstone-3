//dependencies
import { useEffect, useState } from "react";
import { Card, Row, Col, Jumbotron, Button, Container } from "react-bootstrap";
import { Link } from 'react-router-dom';


//this component will generate the feature product banner
export default function Featured_Comp({featuredProp}){

    //set states for the banner info
    const [img, setImg] = useState('');
    const [name, setName] = useState('');
    const [category, setCategory] = useState('');
    const [price, setPrice] = useState(0);
    const [id, setId] = useState('');
    const [description, setDescription] = useState('');

    //useeffect waits for the prop to have data, it the prop fetch the data it will re render with the new values assign to the setters
    useEffect(() => {
        if(featuredProp.length !== 0){
            //generate the image url by converting binary data fetch from db to ascii
            const arrayBufferToBase64 = (buffer) => {
                var binary = '';
                var bytes = [].slice.call(new Uint8Array(buffer));
                bytes.forEach((b) => binary += String.fromCharCode(b));
                return window.btoa(binary);
            };

            //assign src string
            const imgSrc = `data:${featuredProp.img.contentType};base64,${arrayBufferToBase64(featuredProp.img.data.data)}`

            //set states if data is fetched
            setImg(imgSrc);
            setName(featuredProp.productName);
            setCategory(featuredProp.productCategory);
            setPrice(featuredProp.productPrice);
            setDescription(featuredProp.productDescription);
            setId(featuredProp._id);

        }
    },[featuredProp]);

    //creates featured product banner
    return(
        <Jumbotron fluid className="catalogue-featured-image">
            <Container>
                
                <Row>
                <Col xs={12} md={6} lg={6}>
                    <h3>Featured Product</h3>
                    <h1 className="catalogue-featured-title-responsive">{name}</h1>
                    <p className="catalogue-featured-caption-responsive catalogue-featured-caption-container mt-5">
                        {description}  
                    </p>
                    <div className="catalogue-featured-button-container mt-5">
                        <Link className="catalogue-featured-button mt-5" to={`/shop/catalogue/${id}`}>Details</Link>
                    </div>
                </Col>
                <Col xs={12} md={6} lg={6}>
                    <div className="catalogue-featured-container">
                        <img src={img} className="catalogue-featured-img"/>
                    </div>
                </Col>
                </Row>
            </Container>
        </Jumbotron>
    );    
}
