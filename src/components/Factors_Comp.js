import { Row, Col, Card, Container } from "react-bootstrap";

import Factor1 from './../assets/factor1.png'
import Factor2 from './../assets/factor2.png'
import Factor3 from './../assets/factor3.png'

export default function Factors_Comp(){
    return(
        <Container className="mt-5 mb-5">
            <div className="factor-header">
                <p className="factor-header-text">3 Factors to Consider When Buying Coffee Beans Online</p> 
            </div>
            <Row>
                <Col xs={12} md={12} lg={4} className="factor-card-container">
                    <Row>
                        <Col xs={12} md={6} md={{order: 'first'}} lg={12}>
                            <div className="factor-image-container">
                                <img src={Factor1} className="factor-image"/>
                            </div>
                        </Col>
                        <Col xs={12} md={6} md={{order: 'last'}} lg={12}>
                            <Card className="card-highlights factor-card">
                                <Card.Body>
                                    <Card.Title className="factor-title">Check the Type of Roast</Card.Title>
                                    <Card.Text className="factor-caption">
                                        The main types of roast are light, medium and dark roast. The roast type is mostly indicated on the packaging. Of the three types, dark roast is the cheapest and of the lowest quality too. Since it is not possible to see or touch the coffee beans online, the packages are usually labeled, ‘dark roast’, for you to know what you are buying.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row> 
                </Col>
                <Col xs={12} md={12} lg={4} className="factor-card-container">
                    <Row>
                        <Col xs={12} md={6} lg={12}>
                            <div className="factor-image-container">
                                <img src={Factor2} className="factor-image"/>
                            </div>
                        </Col>
                        <Col xs={12} md={6} lg={12}>
                            <Card className="card-highlights factor-card">
                                <Card.Body>
                                    <Card.Title className="factor-title">Check the Roast Date</Card.Title>
                                    <Card.Text className="factor-caption">
                                        Fresh coffee beans make much more coffee quality and you should, therefore, check the roast date before you buy coffee beans online. As coffee beans have a longer shelf-life, most people hardly check the roast date when ordering online. This should not be the case, as the more fresh beans you order, the better the quality of coffee you will make.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row> 
                </Col>
                <Col xs={12} md={12} lg={4} className="factor-card-container">
                    <Row>
                        <Col xs={12} md={6} md={{order: 'first'}} lg={12}>
                            <div className="factor-image-container">
                                <img src={Factor3} className="factor-image"/>
                            </div>
                        </Col>
                        <Col xs={12} md={6} md={{order: 'last'}} lg={12}>
                            <Card className="card-highlights factor-card">
                                <Card.Body>
                                    <Card.Title className="factor-title">Choose Between Whole / Ground</Card.Title>
                                    <Card.Text className="factor-caption">
                                        If you prefer to brew coffee on your own, you should go for the whole beans as they will give you a more quality taste of coffee. Most people ordering online prefer to buy whole beans as they enjoy the brewing process involved. You can, however, order ground coffee online if you prefer a far much easier and quicker process of making your coffee.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row> 
                </Col>
            </Row>
        </Container>
    );
}