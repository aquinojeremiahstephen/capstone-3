import { useEffect, useState } from "react";
import { Card, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';

//this component will generate the product cards
export default function Product_Comp({productProp}){
    const { _id, productName, productDescription, productCategory, productPrice, img } = productProp;

    //generate the image url by converting binary data fetch from db to ascii
    const arrayBufferToBase64 = (buffer) => {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    };
    
    //assign src string
    const imgSrc = `data:${img.contentType};base64,${arrayBufferToBase64(img.data.data)}`

    //creates card
    return(
        <Col xs={6} md={6} lg={3}>
            <Card className="catalogue-card-container">
                <div className="catalogue-card-img-container">
                    <Card.Img className="catalogue-card-img" src={imgSrc} />
                </div>
                <Card.Body className="catalogue-card-details-container">
                    <Card.Title className="catalogue-card-title">{productName}</Card.Title>
                    <p className="catalogue-card-category">{productCategory}</p>
                    <p className="catalogue-card-price">PHP {productPrice}.00</p>
                    <div className="catalogue-card-button-container">
                        <Link className="catalogue-card-button" to={`/shop/catalogue/${_id}`}>Order Now</Link>
                    </div>
                </Card.Body>
            </Card>
        </Col>
    );
}