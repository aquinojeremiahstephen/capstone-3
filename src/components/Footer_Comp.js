import { Jumbotron, Container } from "react-bootstrap";

export default function Footer_Comp(){

    return(
        <div className="footer-container">
            <p>@Copyright Brewing Panda. All Rights Reserved<br></br>Powered by MERN Tech Stack</p>
        </div>
    );
}