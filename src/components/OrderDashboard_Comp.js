//dependencies
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Form, Row, Col, Table, ButtonGroup } from 'react-bootstrap';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
//usercontext
import UserContext from '../UserContext';

//Creates the specific product page
export default function OrderDashboard_Comp(){

    //set state for rendering, setter will set when a data is successfuly fetched
    const [toConfirmOrders, setToConfirmOrders] = useState([]);
    const [toShipOrders, setToShipOrders] = useState([]);
    const [shippedOrders, setShippedOrders] = useState([]);
    const [deliveredOrders, setDeliveredOrders] = useState([]);
    const [cancelledOrders, setCancelledOrders] = useState([]);

    //set state for the orders array to be passed as props
    const [toConfirmItems, setToConfirmItems] = useState([]);
    const [toShipItems, setToShipItems] = useState([]);
    const [shippedItems, setShippedItems] = useState([]);
    const [deliveredItems, setDeliveredItems] = useState([]);
    const [cancelledItems, setCancelledItems] = useState([]);

    //function for fetching to confirm orders
    const fetchToConfirmOrders = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/toConfirm`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setToConfirmOrders(data);
        })
    }

    //function for fetching to ship orders
    const fetchToShipOrders = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/toShip`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setToShipOrders(data);
        })
    }

    //function for fetching to shipped orders
    const fetchShippedOrders = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/shipped`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setShippedOrders(data);
        })
    }

    //function for fetching to delivered orders
    const fetchDeliveredOrders = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/delivered`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setDeliveredOrders(data);
        })
    }

    //function for fetching to shipped orders
    const fetchCancelledOrders = () =>{
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/cancelled`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setCancelledOrders(data);
        })
    }

    //function to set order status to to confirm
    const updateOrderStatusToConfirm = (orderId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/update/toConfirm/${orderId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, 
            if(data === true){
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order successfully updated"
                });
            }else{
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //function to set order status to to ship
    const updateOrderStatusToShip = (orderId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/update/toShip/${orderId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, 
            if(data === true){
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order successfully updated"
                });
            }else{
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //function to set order status to shipped
    const updateOrderStatusShipped = (orderId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/update/shipped/${orderId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, 
            if(data === true){
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order successfully updated"
                });
            }else{
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //function to set order status to delivered
    const updateOrderStatusDelivered = (orderId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/update/delivered/${orderId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, 
            if(data === true){
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order successfully updated"
                });
            }else{
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //function to set order status to shipped
    const updateOrderStatusCancelled = (orderId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL }/admin/orders/update/cancelled/${orderId}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            //call fetchData upon recieving the response from the server so we can repopulate the data, 
            if(data === true){
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order successfully updated"
                });
            }else{
                //render table after changes
                fetchToConfirmOrders();
                fetchToShipOrders();
                fetchShippedOrders();
                fetchDeliveredOrders();
                fetchCancelledOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
            }
        })
    }

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'orders state' to get orders
        //this will build the table
        const toConfirmOrdersArr = toConfirmOrders.map(order => {
            let orderRefNo = `BRWPN${order._id.substring(0,8)}`;
            let orderShortDate = order.orderDate.substring(0,10);
            //extract item summary
            let orderSummary = order.orderSummary.map(item => {
                return(
                    <p>{item.productName} - x{item.productQty}</p>
                );
                
            })
            return(
                <tr key={order._id}>
                    <td>{orderRefNo}</td>
                    <td>{order.orderBuyerFname} {order.orderBuyerLname}</td>
                    <td>{orderShortDate}</td>
                    <td>{orderSummary}</td>
                    <td>PHP {order.orderTotal}.00</td>
                    <td className="dashboard-order-btn-container">
                        <ButtonGroup vertical className="dashboard-order-button">
                            <Button className="dashboard-order-button" size="sm" variant="success" onClick={() => updateOrderStatusToConfirm(order._id)}>To confirm</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToShip(order._id)}>To ship</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusShipped(order._id)}>Shipped</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusDelivered(order._id)}>Delivered</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusCancelled(order._id)}>Cancelled</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of <td> to build the table
        setToConfirmItems(toConfirmOrdersArr);
    },[toConfirmOrders])

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'orders state' to get orders
        //this will build the table
        const toShipOrdersArr = toShipOrders.map(order => {
            let orderRefNo = `BRWPN${order._id.substring(0,8)}`;
            let orderShortDate = order.orderDate.substring(0,10);
            //extract item summary
            let orderSummary = order.orderSummary.map(item => {
                return(
                    <p>{item.productName} - x{item.productQty}</p>
                );
                
            })
            return(
                <tr key={order._id}>
                    <td>{orderRefNo}</td>
                    <td>{order.orderBuyerFname} {order.orderBuyerLname}</td>
                    <td>{orderShortDate}</td>
                    <td>{orderSummary}</td>
                    <td>PHP {order.orderTotal}.00</td>
                    <td className="dashboard-order-btn-container">
                        <ButtonGroup vertical>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToConfirm(order._id)}>To confirm</Button>
                            <Button className="dashboard-order-button" size="sm" variant="success" onClick={() => updateOrderStatusToShip(order._id)}>To ship</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusShipped(order._id)}>Shipped</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusDelivered(order._id)}>Delivered</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusCancelled(order._id)}>Cancelled</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of <td> to build the table
        setToShipItems(toShipOrdersArr);
    },[toShipOrders])

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'orders state' to get orders
        //this will build the table
        const shippedOrdersArr = shippedOrders.map(order => {
            let orderRefNo = `BRWPN${order._id.substring(0,8)}`;
            let orderShortDate = order.orderDate.substring(0,10);
            //extract item summary
            let orderSummary = order.orderSummary.map(item => {
                return(
                    <p>{item.productName} - x{item.productQty}</p>
                );
                
            })
    
            return(
                <tr key={order._id}>
                    <td>{orderRefNo}</td>
                    <td>{order.orderBuyerFname} {order.orderBuyerLname}</td>
                    <td>{orderShortDate}</td>
                    <td>{orderSummary}</td>
                    <td>PHP {order.orderTotal}.00</td>
                    <td className="dashboard-order-btn-container">
                        <ButtonGroup vertical>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToConfirm(order._id)}>To confirm</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToShip(order._id)}>To ship</Button>
                            <Button className="dashboard-order-button" size="sm" variant="success" onClick={() => updateOrderStatusShipped(order._id)}>Shipped</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusDelivered(order._id)}>Delivered</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusCancelled(order._id)}>Cancelled</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of <td> to build the table
        setShippedItems(shippedOrdersArr);
    },[shippedOrders])

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'orders state' to get orders
        //this will build the table
        const deliveredOrdersArr = deliveredOrders.map(order => {
            let orderRefNo = `BRWPN${order._id.substring(0,8)}`;
            let orderShortDate = order.orderDate.substring(0,10);
            //extract item summary
            let orderSummary = order.orderSummary.map(item => {
                return(
                    <p>{item.productName} - x{item.productQty}</p>
                );
                
            })
    
            return(
                <tr key={order._id}>
                    <td>{orderRefNo}</td>
                    <td>{order.orderBuyerFname} {order.orderBuyerLname}</td>
                    <td>{orderShortDate}</td>
                    <td>{orderSummary}</td>
                    <td>PHP {order.orderTotal}.00</td>
                    <td className="dashboard-order-btn-container">
                        <ButtonGroup vertical>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToConfirm(order._id)}>To confirm</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToShip(order._id)}>To ship</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusShipped(order._id)}>Shipped</Button>
                            <Button className="dashboard-order-button" size="sm" variant="success" onClick={() => updateOrderStatusDelivered(order._id)}>Delivered</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusCancelled(order._id)}>Cancelled</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of <td> to build the table
        setDeliveredItems(deliveredOrdersArr);
    },[deliveredOrders])

    //use effect, if the data from 'toConfirmOrders state' changes this will set a new array value for 'orders' usestate
    useEffect(() => {
        //when the this component renders it will loop for each item on 'orders state' to get orders
        //this will build the table
        const cancelledOrdersArr = cancelledOrders.map(order => {
            let orderRefNo = `BRWPN${order._id.substring(0,8)}`;
            let orderShortDate = order.orderDate.substring(0,10);
            //extract item summary
            let orderSummary = order.orderSummary.map(item => {
                return(
                    <p>{item.productName} - x{item.productQty}</p>
                );
                
            })
    
            return(
                <tr key={order._id}>
                    <td>{orderRefNo}</td>
                    <td>{order.orderBuyerFname} {order.orderBuyerLname}</td>
                    <td>{orderShortDate}</td>
                    <td>{orderSummary}</td>
                    <td>PHP {order.orderTotal}.00</td>
                    <td className="dashboard-order-btn-container">
                        <ButtonGroup vertical>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToConfirm(order._id)}>To confirm</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusToShip(order._id)}>To ship</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusShipped(order._id)}>Shipped</Button>
                            <Button className="dashboard-order-button" size="sm" variant="secondary" onClick={() => updateOrderStatusDelivered(order._id)}>Delivered</Button>
                            <Button className="dashboard-order-button" size="sm" variant="danger" onClick={() => updateOrderStatusCancelled(order._id)}>Cancelled</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });
        //after the loop it will create an array of <td> to build the table
        setCancelledItems(cancelledOrdersArr);
    },[cancelledOrders])

    //refetch if reloaded
    useEffect(() => {
        fetchToConfirmOrders();
        fetchToShipOrders();
        fetchShippedOrders();
        fetchDeliveredOrders();
        fetchCancelledOrders();
    },[]);

    
    return(
        <Container>
            <Tabs>
                <TabList>
                    <Tab>To confirm</Tab>
                    <Tab>To ship</Tab>
                    <Tab>Shipped</Tab>
                    <Tab>Delivered</Tab>
                    <Tab>Cancelled</Tab>
                </TabList>
                    
                <TabPanel>
                    <Table striped bordered hover responsive >
                        <thead className="dashboard-orders-thead">
                            <th>Order No</th>
                            <th>Buyer</th>
                            <th>Placed on</th>
                            <th>Order Summary</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                        </thead>
                        <tbody className="dashboard-orders-tbody">
                            {toConfirmItems}
                        </tbody>
                    </Table>
                </TabPanel>
                <TabPanel>
                    <Table striped bordered hover responsive >
                        <thead className="dashboard-orders-thead">
                            <th>Order No</th>
                            <th>Buyer</th>
                            <th>Placed on</th>
                            <th>Order Summary</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                        </thead>
                        <tbody className="dashboard-orders-tbody">
                            {toShipItems}
                        </tbody>
                    </Table>
                </TabPanel>
                <TabPanel>
                    <Table striped bordered hover responsive >
                        <thead className="dashboard-orders-thead">
                            <th>Order No</th>
                            <th>Buyer</th>
                            <th>Placed on</th>
                            <th>Order Summary</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                        </thead>
                        <tbody className="dashboard-orders-tbody">
                            {shippedItems}
                        </tbody>
                    </Table>
                </TabPanel>
                <TabPanel>
                    <Table striped bordered hover responsive >
                        <thead className="dashboard-orders-thead">
                            <th>Order No</th>
                            <th>Buyer</th>
                            <th>Placed on</th>
                            <th>Order Summary</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                        </thead>
                        <tbody className="dashboard-orders-tbody">
                            {deliveredItems}
                        </tbody>
                    </Table>
                </TabPanel>
                <TabPanel>
                    <Table striped bordered hover responsive >
                        <thead className="dashboard-orders-thead">
                            <th>Order No</th>
                            <th>Buyer</th>
                            <th>Placed on</th>
                            <th>Order Summary</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                        </thead>
                        <tbody className="dashboard-orders-tbody">
                            {cancelledItems}
                        </tbody>
                    </Table>
                </TabPanel>
            </Tabs>
        </Container>
    );
}