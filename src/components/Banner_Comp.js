//dependencies
import { Jumbotron, Button, Container } from "react-bootstrap";
import { Link, useHistory } from 'react-router-dom';

export default function Banner_Comp(){

    return(
        <Jumbotron fluid className="banner-image">
            <Container>
                <h1 className="banner-title-responsive">Brewing Panda</h1>
                <p className="banner-caption-responsive banner-caption-container mt-5">
                    Caffeine goodness from farm to table. Enjoy affordable organic coffee beans fresh from plantation, just a click away  
                </p>
                <div className="banner-button-container mt-5">
                    <Link variant="primary" className="banner-button mt-5" to="/shop">
                        Lets roast!
                    </Link>
                </div>
            </Container>
        </Jumbotron>
    );
}