//dependencies
import { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';
//components
import Products_Comp from './Products_Comp';

//This component will generate the products 
export default function Catalogue_Comp({productsData}){

    //productData prop is passed from Catalogue_Page.js

    //set products state. with setProducts setter
    const [products, setProducts] = useState([]);

    //use effect, if the data from 'productsData' prop changes this will set a new array value for 'products' usestate
    useEffect(() => {

        //when the this component renders it will loop for each item on 'productsData' to get active products
        //the loop will repeat if the productsData prpop changes its value
        const productsArr = productsData.map(product => {
            if(product.productStatus === "Active"){
                return(
                    <Products_Comp productProp={product} key={product._id}/>
                )
            }else{
                return null;
            }
            
        });
        //after the loop it will create an array of components using 'Product_Comp' to generate product cards
        setProducts(productsArr);

    },[productsData])

    //Creates the Catalouge Page via Product_Comp
    return(
        <Container className="catalogue-main-card-container">
            <Row>
                {products}
            </Row>
        </Container>
    );
}