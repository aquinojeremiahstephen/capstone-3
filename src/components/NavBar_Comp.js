//dependencies
import { useContext, useEffect, useState } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
//css
import './../css/style.css';
//assets
import Logo from './../assets/logo.png';
//usercontext
import UserContext from './../UserContext';

//Component for Nav_Bar linking
export default function NavBar_Comp() {

    //global user state + unsetUser function from App.js
    const { user, unsetUser } = useContext(UserContext);
    const history = useHistory();
    const [rightMenu, setRightMenu] = useState(undefined);

    //function that calls unsetUser() function to clear global user state
    const logout = () => {
        unsetUser()
        history.push('/login');
    }
    
    useEffect(() => {
        //conditional rendering for navbar is the user is logged in
        let rightNav = (!user.id) ? (
            <>
                <Link className="nav-link" to="/register">Register</Link>
                <Link className="nav-link" to="/login">Login</Link>
            </>
        ) : (

            (!user.userIsAdmin) ? (
                <>
                    <NavDropdown variant="dark" title={`Hello, ${user.userFname}`} id="collasible-nav-dropdown" className="nav-dropdown-container">
                        <NavDropdown.Item><Link className="nav-link" to="/cart">Cart</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link className="nav-link" to="/orders">Order</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link className="nav-link" onClick={logout}>Log Out</Link></NavDropdown.Item>
                    </NavDropdown>
                </>
            )
            :
            (
                <>
                    <Link className="nav-link" to="/dashboard">Dashboard</Link>
                    <Link className="nav-link" onClick={logout}>Log Out</Link>
                </>
            )
            
        )

        setRightMenu(rightNav);
    },[user]);
    
    //creates nav bar
    return(
        <>
            <Navbar sticky="top" collapseOnSelect expand="lg" variant="dark" className="nav-padding nav-background-color">
                    <Navbar.Brand>
                        <img
                        alt=""
                        src={Logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' '}
                        Brewing Panda
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto navbar-nav">
                            <Link className="nav-link" to="/">Home</Link>
                            <Link className="nav-link" to="/shop">Shop</Link>
                        </Nav>
                        <Nav className="ml-auto navbar-nav">
                            {rightMenu}
                        </Nav>
                    </Navbar.Collapse>
            </Navbar>
        </>
    );
}