import { Jumbotron, Container } from "react-bootstrap";

export default function About_COmp(){

    return(
        <Jumbotron fluid className="about-image">
            <Container>
                <h1 className="about-title-responsive">Our Story</h1>
                <p className="about-caption-responsive about-caption-container mt-1">
                    We the Brewing Panda Co. serves fresh and organic coffee beans online. We are online driven where customers buys our beans online on any electronic platform where they become part of our story.  It starts with the fresh beans, wholesome and organic.  Our selection is simple and elegant.  We don't do much besides coffee beans, so we're really good at it. We gurantee our customer fresh produce from our farms to their table. 
                </p>
            </Container>
    </Jumbotron>
    );
}