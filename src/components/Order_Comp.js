//dependencies
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Form, Row, Col } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
//usercontext
import UserContext from './../UserContext';

//Creates the specific product page
export default function Order_Comp({ordersProp}){

    //destructure props for easy assignment
    const { _id, orderBuyerId, orderDate, orderStatus, orderTotal, orderSummary } = ordersProp;

    const [orderDetails, setOrderDetails] = useState([]);

    let orderRefNo = `BRWPN${_id.substring(0,8)}`;
    let orderShortDate = orderDate.substring(0,10);

    const orderDetailsArr = orderSummary.map(order => {
        return(
            <li>{order.productName} - X{order.productQty}</li>
        )
    });

    return(
    <Col sm={12} md={4} lg={3}>
        <Card className="mb-3">
            <Card.Header className="order-card-header">Order No: {orderRefNo}</Card.Header>
            <Card.Body>
                <Card.Title className="order-card-date">Order date: {orderShortDate}</Card.Title>
                <Card.Text>
                    <ul className="order-card-details">
                        {orderDetailsArr}
                    </ul>
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-muted">{orderStatus}</Card.Footer>
        </Card>
    </Col>    
    );
}