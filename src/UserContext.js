import React from 'react';

//create a Context object
const UserContext = React.createContext()

//UserProvider isa a component that the useContext hook can "consume" to suscribe to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;